﻿using Mattsnippets.EventSystem;
using System.Collections;
using UnityEngine;

/// <summary>
/// Manager class for the cinematic camera. Used for climbing animations.
/// </summary>
public class CinematicCamManager : MonoBehaviour
{
    [SerializeField]
    private CharacterController characterController;
    [SerializeField]
    private Camera gunCamera;

    private Camera cinematicCamera;
    private Animator cinematicCamAnimator;
    private ClimbUpStartEvent climbUpStartedEvent = new ClimbUpStartEvent();
    private ClimbUpFinishEvent climbUpFinishedEvent = new ClimbUpFinishEvent();

    /// <summary>
    /// Add event handler for climbing event.
    /// </summary>
    private void OnEnable()
    {
        ClimbUpTriggerEvent.AddListener(OnClimbUpTrigger);
    }

    /// <summary>
    /// Remove event handler for climbing event.
    /// </summary>
    private void OnDisable()
    {
        ClimbUpTriggerEvent.RemoveListener(OnClimbUpTrigger);
    }

    /// <summary>
    /// Get camera and animator references.
    /// </summary>
    private void Start()
    {
        cinematicCamera = GetComponent<Camera>();
        cinematicCamAnimator = GetComponent<Animator>();
    }

    /// <summary>
    /// Event handler for climbing. Enable cinematic cam, trigger climb up animation. Disable character
    /// controller and gun camera, broadcast ClimbUpStarted event.
    /// </summary>
    private void OnClimbUpTrigger(ClimbUpTriggerEvent eventData)
    {
        cinematicCamera.enabled = true;
        cinematicCamera.depth = 3f;
        cinematicCamAnimator.SetTrigger("ClimbUp");

        characterController.enabled = false;
        climbUpStartedEvent.Fire();
    }

    /// <summary>
    /// Method called at the end of climb up animation. Move character controller to the new position,
    /// start coroutine WaitBeforeCharacterControllerEnable().
    /// </summary>
    public void MoveCharacterToCinematicPosition()
    {
        characterController.transform.position =
            new Vector3(transform.position.x,
            transform.position.y - characterController.height,
            transform.position.z);

        StartCoroutine(WaitBeforeCharacterControllerEnable());
    }

    /// <summary>
    /// Disable cinematic cam, enable gun cam, broadcast ClimbUpFinished event, wait a bit, then enable
    /// character controller.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator WaitBeforeCharacterControllerEnable()
    {
        cinematicCamera.enabled = false;
        cinematicCamera.depth = 0f;

        climbUpFinishedEvent.Fire();

        yield return new WaitForSeconds(0.2f);

        characterController.enabled = true;
    }    
}
