﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Crouch controller class. Checks if changing standing/crouching position is possible.
/// </summary>
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class CrouchController : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float length;
        [SerializeField]
        private float crouchingHeight;
        [SerializeField]
        private float standingHeight;
        [SerializeField]
        private LayerMask layerMask;
        [SerializeField]
        private float camStandingHeight;
        [SerializeField]
        private float camCrouchingHeight;
        [SerializeField]
        private AnimationCurve animationCurve;

        private Camera mainCamera;
        private CharacterController characterController;
        private bool isCrouching;
        private Coroutine crouchTransition;

        /// <summary>
        /// Get character controller and camera references
        /// </summary>
        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
            mainCamera = Camera.main;
        }

        /// <summary>
        /// When getting crouch input check if position change is possible. If yes, change between 
        /// crouching/standing position.
        /// </summary>
        private void Update()
        {
            if (CrossPlatformInputManager.GetButtonDown("Crouch") && CanChangePosition() && crouchTransition == null)
            {
                if (isCrouching)
                {
                    isCrouching = false;
                    characterController.height = standingHeight;
                    characterController.center = new Vector3(0f, 1f, 0f);
                    crouchTransition = StartCoroutine(CrouchTransition(camCrouchingHeight, camStandingHeight));
                }
                else
                {
                    isCrouching = true;
                    characterController.height = crouchingHeight;
                    characterController.center = new Vector3(0f, 0.5f, 0f);
                    crouchTransition = StartCoroutine(CrouchTransition(camStandingHeight, camCrouchingHeight));
                }
            }
        }

        /// <summary>
        /// Coroutine lerping the camera between start and target height
        /// </summary>
        /// <param name="startHeight">Start height</param>
        /// <param name="targetHeight">Target height</param>
        /// <returns>Coroutine</returns>
        private IEnumerator CrouchTransition(float startHeight, float targetHeight)
        {
            float elapsedTime = 0f;

            while (elapsedTime < length)
            {
                mainCamera.transform.position = new Vector3(mainCamera.transform.position.x,
                    Mathf.Lerp(characterController.transform.position.y + startHeight,
                    characterController.transform.position.y + targetHeight,
                    animationCurve.Evaluate(elapsedTime / length)), mainCamera.transform.position.z);
                elapsedTime += Time.deltaTime * speed;
                yield return null;
            }

            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x,
                   characterController.transform.position.y + targetHeight, mainCamera.transform.position.z);

            crouchTransition = null;
        }

        /// <summary>
        /// Check if character is allowed to crouch or stand up.
        /// </summary>
        /// <returns>True if character is allowed to change position</returns>
        private bool CanChangePosition()
        {
            // If not grounded, can't change position
            if (!characterController.isGrounded)
            {
                return false;
            }

            // If trying to stand up, but space above character is obstructed, can't stand up
            if (isCrouching)
            {
                return Physics.SphereCastNonAlloc(new Vector3(characterController.transform.position.x,
                    characterController.transform.position.y + characterController.radius,
                    characterController.transform.position.z), characterController.radius,
                    transform.up, new RaycastHit[2], standingHeight - (2f * characterController.radius), layerMask) == 0;
            }
            else
            {
                return true;
            }
        }
    }
}