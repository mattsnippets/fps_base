﻿Shader "Custom/Transparent Color" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader{

			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
		Pass{
		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;

	fixed4 frag(v2f_img i) : SV_Target{
		fixed4 r = tex2D(_MainTex, i.uv);

		return r;
	}
		ENDCG
	}
	}
}