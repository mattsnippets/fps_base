﻿using System;
using UnityEngine;

/// <summary>
/// State controller class for PluggableAI.
/// </summary>
public class StateController : MonoBehaviour
{
    [SerializeField]
    private State currentState;
    [SerializeField]
    private State remainState;
    [SerializeField]
    private Transform chaseTarget;
    [SerializeField]
    private bool aiActive = true;

    private float stateTimeElapsed;
    private MobSoundController mobSoundController;

    public FieldOfView FieldOfView { get; private set; }

    public MobWeaponController MobWeaponController { get; private set; }

    public MobMoveController MobMoveController { get; private set; }

    public MobHealthManager MobHealthManager { get; private set; }

    public Transform ChaseTarget
    {
        get
        {
            return chaseTarget;
        }
    }

    /// <summary>
    /// Set component references.
    /// </summary>
    private void Awake()
    {
        FieldOfView = GetComponent<FieldOfView>();
        MobMoveController = GetComponent<MobMoveController>();
        MobWeaponController = GetComponent<MobWeaponController>();
        mobSoundController = GetComponent<MobSoundController>();
        MobHealthManager = GetComponent<MobHealthManager>();
        MobMoveController.Target = chaseTarget;
        currentState.OnEnter(this);
    }

    /// <summary>
    /// Call UpdateState() on current state.
    /// </summary>
    private void FixedUpdate()
    {
        if (aiActive)
        {
            currentState.UpdateState(this);
        }
    }

    /// <summary>
    /// Show gizmo with the appropriate color of the current state.
    /// </summary>
    private void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(gameObject.transform.position, 2f);
        }
    }

    /// <summary>
    /// Transition to state and play sound clip associated with next state.
    /// </summary>
    /// <param name="nextState"></param>
    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            Debug.Log(string.Format("Mob changing state, current: {0}, next: {1}", currentState.name, nextState.name));
            stateTimeElapsed = 0f;
            currentState.OnExit();
            currentState = nextState;
            currentState.OnEnter(this);
            mobSoundController.PlaySound((int)Enum.Parse(typeof(DroneSound), nextState.name));
        }
    }

    /// <summary>
    /// Check if time spent in the current state elapsed a certain duration/
    /// </summary>
    /// <param name="duration">Duration</param>
    /// <returns>True if time has exceeded the duration</returns>
    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return stateTimeElapsed >= duration;
    }
}