﻿using UnityEngine;

/// <summary>
/// Class representing a state in the PluggableAI FSM.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject
{
    public AiAction[] actions;
    public Transition[] transitions;
    public Color sceneGizmoColor = Color.gray;
    [SerializeField]
    private AiAction enterAction;
    [SerializeField]
    private AiAction exitAction;

    private StateController controller;

    /// <summary>
    /// Called when this state is entered, if an enter action is set, it is performed here.
    /// </summary>
    /// <param name="controller">Reference to the StateController object</param>
    public void OnEnter(StateController controller)
    {
        this.controller = controller;
        enterAction?.Act(this.controller);
    }

    /// <summary>
    /// Called when this state is exited, if an exit action is set, it is performed here.
    /// </summary>
    public void OnExit()
    {
        exitAction?.Act(controller);
    }

    /// <summary>
    /// Do all actions and check transitions.
    /// </summary>
    /// <param name="controller"></param>
    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    /// <summary>
    /// Call every action's Act() method.
    /// </summary>
    /// <param name="controller">StateController reference</param>
    private void DoActions(StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    /// <summary>
    /// Check every transition by calling its decision's Decide() method.
    /// </summary>
    /// <param name="controller">StateController reference</param>
    private void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }
}
