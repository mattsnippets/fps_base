﻿using UnityEngine;

/// <summary>
/// Abstract decision class of pluggable AI.
/// </summary>
public abstract class Decision : ScriptableObject
{
    public abstract bool Decide(StateController controller);
}
