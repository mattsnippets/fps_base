﻿using UnityEngine;

/// <summary>
/// Class representing scan for target AI decision.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Decisions/Scan")]
public class ScanDecision : Decision
{
    [SerializeField]
    private float scanTurningSpeed;
    [SerializeField]
    private float scanDuration;

    public override bool Decide(StateController controller)
    {
        bool isTargetGone = Scan(controller);
        return isTargetGone;
    }

    private bool Scan(StateController controller)
    {
        controller.MobWeaponController.StopAttack();
        controller.MobMoveController.Stop();
        controller.transform.Rotate(0, scanTurningSpeed * Time.deltaTime, 0);
        return controller.CheckIfCountDownElapsed(scanDuration);
    }
}
