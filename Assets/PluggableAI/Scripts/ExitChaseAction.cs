﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/ExitChase")]
public class ExitChaseAction : AiAction
{
    public override void Act(StateController controller)
    {
        controller.MobMoveController.IsAttacking = false;
    }
}
