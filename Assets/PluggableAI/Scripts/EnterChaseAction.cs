﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/EnterChase")]
public class EnterChaseAction : AiAction
{
    public override void Act(StateController controller)
    {
        controller.MobMoveController.IsAttacking = true;
    }
}
