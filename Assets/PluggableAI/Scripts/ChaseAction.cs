﻿using UnityEngine;

/// <summary>
/// Class representing the chase AI action.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase")]
public class ChaseAction : AiAction
{
    public override void Act(StateController controller)
    {
        Chase(controller);
    }

    private void Chase(StateController controller)
    {
        controller.MobMoveController.MoveToDestination(controller.ChaseTarget.position, 8f);
    }
}
