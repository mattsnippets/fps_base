﻿using System.Linq;
using UnityEngine;

/// <summary>
/// Class representing look for visible targets decision for pluggable AI.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look")]
public class LookDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    private bool Look(StateController controller)
    {
        return (controller.FieldOfView.GetVisibleTargets().Any());
    }
}
