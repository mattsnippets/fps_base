﻿using Mattsnippets.EventSystem;
using UnityEngine;

public enum PickupType
{
    Health,
    Ammo
}

/// <summary>
/// Class representing health and ammo pickups
/// </summary>
public class Pickup : MonoBehaviour
{
    [SerializeField]
    private PickupType type;
    [SerializeField]
    private int amount;

    /// <summary>
    /// Fire pickup event and deactivate object on collision
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        new PickupEvent(type, amount).Fire();
        gameObject.SetActive(false);
    }
}
