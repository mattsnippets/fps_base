﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Searchlight behavior script.
/// </summary>
public class Searchlight : MonoBehaviour
{
    [SerializeField]
    private float rotationArc = 180f;
    [SerializeField]
    private float rotationHalfTurnTime = 5f;
    [SerializeField]
    [ColorUsage(true, true)]
    private Color idleColor;
    [SerializeField]
    [ColorUsage(true, true)]
    private Color alertColor;
    [SerializeField]
    private GameObject dish;
    [SerializeField]
    private float targetTurnSpeed = 2f;

    private readonly WaitForSeconds turnPause = new WaitForSeconds(0.2f);
    private float rotationStart;
    private float rotationEnd;
    private float initialRotation;
    private Material dishMaterial;
    private bool isAlerted;
    private Transform target;
    private Quaternion lookRotation;
    private Vector3 targetDir;

    /// <summary>
    /// Set searchlight to alert state: change color, set target reference, start target follow coroutine
    /// </summary>
    /// <param name="target">Transform of detected target</param>
    public void SetAlert(Transform target)
    {
        dishMaterial.SetColor("_EmissionColor", alertColor);
        StopAllCoroutines();
        this.target = target;
        StartCoroutine(UpdateTargetLookRotation());
        isAlerted = true;
        // Debug.Log("Rotation scan stopped, turning towards target.");
    }

    /// <summary>
    /// Set searchlight to idle state: set color, stop target follow coroutine, reset rotation to idle scan
    /// </summary>
    public void SetIdle()
    {
        // Debug.Log("Target lost, set to idle.");
        dishMaterial.SetColor("_EmissionColor", idleColor);
        isAlerted = false;
        StopAllCoroutines();
        StartCoroutine(ResetRotation());
    }

    /// <summary>
    /// Calculate a look rotation towards detected target
    /// </summary>
    private void SetTargetLookRotation()
    {
        targetDir = (target.position - transform.position).normalized;
        targetDir.y = 0;
        lookRotation = Quaternion.LookRotation(targetDir);
    }

    /// <summary>
    /// If alerted, slerp towards target to follow its movement
    /// </summary>
    private void Update()
    {
        Debug.DrawRay(transform.position, targetDir, Color.magenta);

        if (isAlerted)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation,
                targetTurnSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Set initial rotation, rotation arc start and end positions, get material reference for color change,
    /// start default scan movement
    /// </summary>
    private void Awake()
    {
        initialRotation = transform.rotation.eulerAngles.y;
        rotationStart = initialRotation - rotationArc / 2f;
        rotationEnd = initialRotation + rotationArc / 2f;
        dishMaterial = dish.GetComponent<MeshRenderer>().material;
        StartCoroutine(RotateScan());
    }

    /// <summary>
    /// Rotate to initial rotation then start default scan movement
    /// </summary>
    /// <returns></returns>
    private IEnumerator ResetRotation()
    {
        float currentRot = transform.rotation.eulerAngles.y;
        // Debug.Log("Resetting to initial rotation.");
        yield return StartCoroutine(RotateInDirection(currentRot, initialRotation, 5f));
        StartCoroutine(RotateScan());
    }

    /// <summary>
    /// Default scan rotation rotating between two arc endpoints
    /// </summary>
    /// <returns></returns>
    private IEnumerator RotateScan()
    {
        // Debug.Log("Turning towards starting rotation");
        yield return StartCoroutine(RotateInDirection(initialRotation, rotationStart, rotationHalfTurnTime));

        while (true)
        {
            // Debug.Log("Turning towards end rotation");
            yield return StartCoroutine(RotateInDirection(rotationStart, rotationEnd, rotationHalfTurnTime * 2f));
            // Debug.Log("Turning towards start rotation");
            yield return StartCoroutine(RotateInDirection(rotationEnd, rotationStart, rotationHalfTurnTime * 2f));
        }
    }

    /// <summary>
    /// Calculate rotation in one direction over the full arc with lerp
    /// </summary>
    /// <param name="startRotation">Start rotation</param>
    /// <param name="targetRotation">Target rotation</param>
    /// <param name="duration">Duration of rotation movement</param>
    /// <returns></returns>
    private IEnumerator RotateInDirection(float startRotation, float targetRotation, float duration)
    {
        float elapsedTime = 0f;
        yield return turnPause;

        while (elapsedTime < duration)
        {
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x,
                Mathf.Lerp(startRotation, targetRotation, elapsedTime / duration), transform.rotation.z));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, targetRotation, transform.rotation.z));
    }

    /// <summary>
    /// Update the target look rotation to follow it
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator UpdateTargetLookRotation()
    {
        while (true)
        {
            SetTargetLookRotation();
            yield return turnPause;
        }
    }
}
