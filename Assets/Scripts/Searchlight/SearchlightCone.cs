﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Class calculating the detection cone of the serchlight.
/// </summary>
public class SearchlightCone : MonoBehaviour
{
    [SerializeField]
    private Searchlight searchlight;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float coneAngle = 15f;

    private VisibilityRayTracer rayTracer;
    private Coroutine checkTargetVisibility;
    private float coneAngleRad;
    private bool targetVisible;

    /// <summary>
    /// Convert the cone angle to radians, get RayTracer of target
    /// </summary>
    private void Awake()
    {
        coneAngleRad = Mathf.Deg2Rad * coneAngle;
        rayTracer = target.GetComponent<VisibilityRayTracer>();
    }

    /// <summary>
    /// Start default target check coroutine
    /// </summary>
    private void Start()
    {
        StartCoroutine(CheckTargetWithinVisibilityCone());
    }

    /// <summary>
    /// Detect if target is inside cone using the dot product of the searchlight forward vector and 
    /// target direction. If it is inside cone, check if it's (partially) visible using the ray tracer.
    /// If target visible set searchlight to alert state and start coroutine to repeatedly check if target
    /// hasn't disappeared
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator CheckTargetWithinVisibilityCone()
    {
        while (true)
        {
            Vector3 targetDir = (target.position - transform.position).normalized;
            float dot = Vector3.Dot(transform.forward, targetDir);
            float angleBetween = Mathf.Acos(dot);

            if (angleBetween <= coneAngleRad && rayTracer.CheckRayTraceVisibility(transform))
            {
                Debug.Log("Searchlight set to alert state.");
                searchlight.SetAlert(target);

                if (checkTargetVisibility == null)
                {
                    checkTargetVisibility = StartCoroutine(CheckTargetVisibility());
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

    /// <summary>
    /// Check if target is still visible. If it is hidden behind something set searchlight to idle and
    /// stop this coroutine
    /// </summary>
    /// <returns></returns>
    private IEnumerator CheckTargetVisibility()
    {
        while (true)
        {
            if (!rayTracer.CheckRayTraceVisibility(transform))
            {
                Debug.Log("Searchlight set to idle state.");
                searchlight.SetIdle();

                if (checkTargetVisibility != null)
                {
                    StopCoroutine(checkTargetVisibility);
                    checkTargetVisibility = null;
                }

                targetVisible = false;
            }
            else
            {
                targetVisible = true;
            }

            yield return new WaitForSeconds(3f);
        }
    }

    /// <summary>
    /// Debug gizmos showing the cone
    /// </summary>
    private void OnDrawGizmos()
    {
        int segments = 8;
        float length = 40f;
        Gizmos.color = targetVisible ? Color.yellow : Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * length);

        for (int i = 0; i < segments; i++)
        {
            Vector3 rot = Quaternion.AngleAxis(coneAngle, transform.right) * transform.forward;
            Vector3 dir = Quaternion.AngleAxis(Mathf.Rad2Deg * (2f * Mathf.PI / segments) * i, transform.forward) * rot;
            Gizmos.DrawLine(transform.position, transform.position + dir * length);
        }
    }
}
