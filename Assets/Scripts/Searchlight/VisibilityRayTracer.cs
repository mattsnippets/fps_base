﻿using UnityEngine;

/// <summary>
/// Detect the visibility of the GameObject by casting lines from a collection of trace points. This 
/// way a character partially hidden behid an obstacle can still be seen
/// </summary>
public class VisibilityRayTracer : MonoBehaviour
{
    [SerializeField]
    private Transform[] tracePoints;
    [SerializeField]
    private LayerMask layerMask;

    public bool CheckRayTraceVisibility(Transform target)
    {
        bool isVisible = false;

        foreach (var tracePoint in tracePoints)
        {
            if (!Physics.Linecast(tracePoint.position, target.position, layerMask))
            {
                Debug.DrawLine(tracePoint.position, target.position, Color.cyan);
                isVisible = true;
            }
        }

        return isVisible;
    }
}
