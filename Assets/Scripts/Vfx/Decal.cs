﻿using UnityEngine;

/// <summary>
/// Decal object to be instantiated on hit surfaces.
/// </summary>
public class Decal : MonoBehaviour
{
    [SerializeField]
    private Texture[] textures;
    [SerializeField]
    private float minSize = 1f;
    [SerializeField]
    private float maxSize = 1f;
    [SerializeField]
    private LayerMask layerMask;

    private MeshRenderer meshRenderer;
    private static float sortingDistance = 0.01f;

    /// <summary>
    /// Get mesh renderer reference
    /// </summary>
    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// Set a random texture, scale and rotation for the decal. Set the sorting distance to avoid
    /// flashing decals because of overlapping. If the decal would be created in an invalid position, disable
    /// </summary>
    private void OnEnable()
    {
        meshRenderer.material.SetTexture("_MainTex", textures[Random.Range(0, textures.Length)]);
        float randomScale = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(randomScale, randomScale, 1f);
        transform.localPosition += transform.forward * sortingDistance;
        transform.Rotate(Vector3.forward, Random.Range(0f, 360f));

        sortingDistance = sortingDistance >= 0.05f ? 0.01f : sortingDistance + 0.001f;

        if (transform.parent == null)
        {
            // Disable decal if it's over the edge of the target
            foreach (var vertex in GetComponent<MeshFilter>().mesh.vertices)
            {
                if (!Physics.Raycast(transform.TransformPoint(vertex), -transform.forward, 1f, layerMask))
                {
                    gameObject.SetActive(false);
                }
            }
        }
        else // If the decal has a door parent, reduce its size
        {
            transform.localScale *= 0.25f;
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.magenta;

    //    foreach (var vertex in GetComponent<MeshFilter>().mesh.vertices)
    //    {
    //        Gizmos.color = Color.magenta;
    //        Gizmos.DrawRay(transform.TransformPoint(vertex), -transform.forward);
    //    }
    //}
}
