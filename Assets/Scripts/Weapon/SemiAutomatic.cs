﻿using UnityEngine;

/// <summary>
/// Class for semi-automatic weapons. Shoot without bullet spread.
/// </summary>
class SemiAutomatic : Weapon
{
    /// <summary>
    /// Shoot a raycast, no spread
    /// </summary>
    protected override void MakeShot()
    {
        weaponManager.ChangeAmmo(-1);

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, gunStats.range,
            gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}

