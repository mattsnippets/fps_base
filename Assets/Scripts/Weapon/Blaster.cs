﻿using UnityEngine;

/// <summary>
/// Class for blaster type weapon using "real" projectile objects instead of raycasts.
/// </summary>
public class Blaster : Weapon
{
    [SerializeField]
    private GameObject projectilePrefab;
    [SerializeField]
    private Transform muzzle;
    [SerializeField]
    private bool isAutomatic;

    /// <summary>
    /// Instantiate projectile prefab, make it look in the direction of the hit point, add spread
    /// if the weapon is automatic.
    /// </summary>
    protected override void MakeShot()
    {
        weaponManager.ChangeAmmo(-1);

        GameObject projectileGo = ObjectPoolManager.Current.GetPooledObject(projectilePrefab);
        projectileGo.transform.position = muzzle.position;
        projectileGo.transform.rotation = Quaternion.LookRotation(fpsCam.transform.forward);

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, gunStats.range,
            gunStats.layerMask))
        {
            projectileGo.transform.LookAt(hit.point);
        }

        if (isAutomatic)
        {
            projectileGo.transform.rotation = CalculateBulletSpread(projectileGo.transform.rotation);
        }

        projectileGo.GetComponent<Projectile>().Init(gunStats.damage, gunStats.hitForce);
        projectileGo.SetActive(true);
    }
}
