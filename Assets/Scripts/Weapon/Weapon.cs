﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Abstract weapon base class.
/// </summary>
public abstract class Weapon : MonoBehaviour
{
    [SerializeField]
    protected GunStats gunStats;
    [SerializeField]
    protected GameObject hitMarkerPrefab;
    [SerializeField]
    protected AudioClip emptyGun;
    [SerializeField]
    private MuzzleFlash muzzleFlash;

    protected WeaponManager weaponManager;
    protected AudioSource audioSource;
    protected Camera fpsCam;
    protected Vector3 rayOrigin;
    protected RaycastHit hit;
    protected bool readyToFire = true;
    protected float? firingSince;
    private Recoil recoil;

    public bool IsAutomatic
    {
        get
        {
            return gunStats.isAutomatic;
        }
    }

    public float Range
    {
        get
        {
            return gunStats.range;
        }
    }

    public float FireRate
    {
        get
        {
            return gunStats.fireRate;
        }
    }

    /// <summary>
    /// Set weapon ready to fire
    /// </summary>
    private void OnEnable()
    {
        readyToFire = true;
    }

    /// <summary>
    /// Assign component references 
    /// </summary>
    private void Awake()
    {
        Debug.Assert(gameObject.layer == LayerMask.NameToLayer("Gun"), "Gun should be on gun layer.");
        fpsCam = GetComponentInParent<Camera>();
        audioSource = GetComponentInParent<AudioSource>();
        weaponManager = GetComponentInParent<WeaponManager>();
        recoil = GetComponent<Recoil>();
    }

    /// <summary>
    /// If gun is ready to fire, invoke shot ray calculation, start recoil, create muzzle flash, play
    /// shot sound.
    /// </summary>
    /// <param name="firingSince">If weapon is automatic, it is the time since the weapon is firing.
    /// Otherwise it's null</param>
    public void Shoot(float? firingSince)
    {
        if (readyToFire)
        {
            if (weaponManager.Ammo > 0)
            {
                readyToFire = false;
                this.firingSince = firingSince;
                recoil.StartRecoil();
                StartCoroutine(SetReadyToFire());
                rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, .0f));
                MakeShot();
                muzzleFlash.gameObject.SetActive(true);
                audioSource.Play();
            }
            else
            {
                readyToFire = false;
                this.firingSince = firingSince;
                StartCoroutine(SetReadyToFire());

                if (firingSince == 0f || firingSince == null)
                {
                    audioSource.PlayOneShot(emptyGun);
                }
            }
        }
    }

    protected abstract void MakeShot();

    /// <summary>
    /// Create bullet spread by rotating shot direction randomly. In the case of automatic weapons,
    /// increase spread over time.
    /// </summary>
    /// <returns>The rotation to be used during the shot</returns>
    protected Quaternion CalculateBulletSpread(Quaternion fireRotation)
    {
        Quaternion randomRotation = Random.rotation;

        float currentSpread;

        if (firingSince == null)
        {
            currentSpread = gunStats.maxBulletSpreadAngle;
        }
        else
        {
            currentSpread = Mathf.Lerp(.0f, gunStats.maxBulletSpreadAngle, (float)firingSince / gunStats.timeUntilMaxSpreadAngle);
        }

        fireRotation = Quaternion.RotateTowards(fireRotation, randomRotation, currentSpread);
        return fireRotation;
    }

    /// <summary>
    /// Wait according to weapon cooldown then set weapon ready to fire again.
    /// </summary>
    /// <returns></returns>
    protected IEnumerator SetReadyToFire()
    {
        yield return new WaitForSeconds(gunStats.fireRate);
        readyToFire = true;
    }

    /// <summary>
    /// If target has HealthManager, apply damage. If it has rigidbody, apply force. Instantiate
    /// hit marker object. Used for raycast (no projectile object) shots.
    /// </summary>
    protected void OnRayCastHit()
    {
        MobHealthManager targetHealthManager = hit.transform.gameObject.GetComponent<MobHealthManager>();

        if (targetHealthManager != null)
        {
            targetHealthManager.Damage(gunStats.damage);
        }

        if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForce(-hit.normal * gunStats.hitForce);
        }

        GameObject hitMarker = ObjectPoolManager.Current.GetPooledObject(hitMarkerPrefab);
        hitMarker.transform.position = hit.point;
        hitMarker.transform.rotation = Quaternion.LookRotation(hit.normal);
        hitMarker.SetActive(true);
    }
}
