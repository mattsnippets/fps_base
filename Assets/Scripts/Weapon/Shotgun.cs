﻿using UnityEngine;

/// <summary>
/// Class for shotgun weapons. Shoot multiple bullets in quick succession with spread.
/// </summary>
class Shotgun : Weapon
{
    [SerializeField]
    private int shotsPerBullet = 10;

    /// <summary>
    /// Shoot raycasts based on shotgun shot number and add spread to them.
    /// </summary>
    protected override void MakeShot()
    {
        firingSince = null;

        for (int i = 0; i < shotsPerBullet; i++)
        {
            if (weaponManager.Ammo > 0)
            {
                weaponManager.ChangeAmmo(-1);

                if (Physics.Raycast(rayOrigin, CalculateBulletSpread(transform.rotation) * Vector3.forward, out hit,
                    gunStats.range, gunStats.layerMask))
                {
                    OnRayCastHit();
                }
            }
        }
    }
}

