﻿using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Weapon projectile class.
/// </summary>
public class Projectile : MonoBehaviour
{
    private const float InitialLifeSpan = 10f;

    [SerializeField]
    private float speed = 50;
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private GameObject decalPrefab;
    [SerializeField]

    private Vector3 lastPosition;
    private float lifeSpan;
    private RaycastHit hit;
    private int damage = -1;
    private float hitForce = -1f;

    /// <summary>
    /// Init projectile hit force and damage values.
    /// </summary>
    /// <param name="damage">Damage value</param>
    /// <param name="hitForce">Hit force value</param>
    public void Init(int damage, float hitForce)
    {
        this.damage = damage;
        this.hitForce = hitForce;
    }

    private void Awake()
    {
        Assert.raiseExceptions = true;
    }

    /// <summary>
    /// Assign current position to last position, assign initial lifespan.
    /// </summary>
    private void OnEnable()
    {
        lastPosition = transform.position;
        lifeSpan = InitialLifeSpan;
    }

    /// <summary>
    /// Subtract elapsed time from lifespan, deactivate when no lifespan left.
    /// </summary>
    private void Update()
    {
        if (lifeSpan <= 0f)
        {
            gameObject.SetActive(false);
        }

        lifeSpan -= Time.deltaTime;
    }

    /// <summary>
    /// Translate the projectile in world space. Do raycast test to detect projectile hit. Disable 
    /// projectile if it hit something.
    /// </summary>
    private void FixedUpdate()
    {
        transform.Translate(transform.forward * Time.deltaTime * speed, Space.World);
        Vector3 direction = transform.position - lastPosition;

        if (Physics.Raycast(lastPosition, direction, out hit, direction.magnitude, layerMask, QueryTriggerInteraction.Ignore))
        {
            OnHit();
            gameObject.SetActive(false);
        }

        lastPosition = transform.position;
    }

    /// <summary>
    /// If target has a health manager, inflict damage otherwise create decal on its surface.
    /// If it has a rigidbody, apply hit force to it.
    /// </summary>
    private void OnHit()
    {
        Assert.IsTrue(damage > -1, "Projectile damage hasn't been set");
        Assert.IsTrue(hitForce > -1, "Projectile hitForce hasn't been set");

        MobHealthManager targetHealthManager = hit.transform.gameObject.GetComponent<MobHealthManager>();

        if (targetHealthManager != null)
        {
            targetHealthManager.Damage(damage);
        }
        else
        {
            CreateDecal();
        }

        if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForce(-hit.normal * hitForce);
        }
    }

    /// <summary>
    /// Instantiate a decal prefab at the hit point, if the target is a door parent the decal to it
    /// (to keep decals on the door object surface).
    /// </summary>
    private void CreateDecal()
    {
        GameObject decal = ObjectPoolManager.Current.GetPooledObject(decalPrefab);
        decal.transform.position = hit.point;
        decal.transform.rotation = Quaternion.LookRotation(hit.normal);

        Tags tags = hit.collider.GetComponent<Tags>();

        if (tags != null && tags.DoorCollider)
        {
            decal.transform.SetParent(hit.transform, true);
        }
        else
        {
            decal.transform.parent = null;
        }

        decal.SetActive(true);
    }
}
