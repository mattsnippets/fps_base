﻿using UnityEngine;

/// <summary>
/// Class for blaster shotgun type weapon using "real" projectile objects instead of raycasts.
/// </summary>
public class BlasterShotgun : Weapon
{
    [SerializeField]
    private GameObject projectilePrefab;
    [SerializeField]
    private Transform muzzle;
    [SerializeField]
    private int shotsPerBullet = 10;

    private Vector3 hitPoint;

    /// <summary>
    /// Instantiate multiple projectiles based on shotgun shot number. Make the projectiles look
    /// in the hit point direction and add spread to them.
    /// </summary>
    protected override void MakeShot()
    {
        firingSince = null;
        hitPoint = Vector3.zero;

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, gunStats.range,
            gunStats.layerMask))
        {
            hitPoint = hit.point;
            // Debug.Log("Raycast hit: " + hit.transform.name);
        }
        else
        {
            // Debug.Log("Raycast no hit");
        }

        for (int i = 0; i < shotsPerBullet; i++)
        {
            if (weaponManager.Ammo > 0)
            {
                weaponManager.ChangeAmmo(-1);
                GameObject projectileGo = ObjectPoolManager.Current.GetPooledObject(projectilePrefab);
                projectileGo.transform.position = muzzle.position;

                if (hitPoint != Vector3.zero)
                {
                    projectileGo.transform.LookAt(hitPoint);
                }
                else
                {
                    projectileGo.transform.rotation = Quaternion.LookRotation(fpsCam.transform.forward);
                }

                projectileGo.transform.rotation = CalculateBulletSpread(projectileGo.transform.rotation);
                projectileGo.GetComponent<Projectile>().Init(gunStats.damage, gunStats.hitForce);
                projectileGo.SetActive(true);
            }
        }
    }
}
