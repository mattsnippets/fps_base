﻿using Mattsnippets.EventSystem;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Player weapon manager class.
/// </summary>
public class WeaponManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject hitMarker;
    [SerializeField]
    private Weapon[] weapons;
    [SerializeField]
    private float gunHolderRotationLength = 1f;
    [SerializeField]
    private AnimationCurve animationCurve;
    [SerializeField]
    private int ammo = 100;

    private float firingSince;
    private int currentWeaponIndex;
    private int prevWeaponIndex;
    private Animator animator;
    private bool isSwitchingWeapon;
    private bool isClimbing;
    private PlayerAmmoChangeEvent ammoChangeEvent;

    public float WeaponRange => weapons[currentWeaponIndex].Range;
    public int Ammo => ammo;

    /// <summary>
    /// Change the amount of ammo, broadcast ammo change message
    /// </summary>
    /// <param name="amount">Amount to apply</param>
    public void ChangeAmmo(int amount)
    {
        ammo += amount;
        ammoChangeEvent.Ammo = ammo;
        ammoChangeEvent.Fire();
    }

    /// <summary>
    /// Get animator reference, init ammo change event
    /// </summary>
    private void Awake()
    {
        animator = GetComponent<Animator>();
        ammoChangeEvent = new PlayerAmmoChangeEvent(ammo);
    }

    /// <summary>
    /// Set active current weapon, broadcast initial ammo count, add ammo pickup event listener
    /// </summary>
    private void Start()
    {
        weapons[currentWeaponIndex].gameObject.SetActive(true);
        ammoChangeEvent.Ammo = ammo;
        ammoChangeEvent.Fire();
    }

    /// <summary>
    /// If ammo pickup event received, increase ammo.
    /// </summary>
    /// <param name="eventData">Event data</param>
    private void OnPickup(PickupEvent eventData)
    {
        if (eventData.Type == PickupType.Ammo)
        {
            ChangeAmmo(eventData.Amount);
        }
    }

    /// <summary>
    /// If firing or changing weapon is possible, check for input accordingly
    /// </summary>
    private void Update()
    {
        if (!isSwitchingWeapon && !isClimbing)
        {
            HandleFireInput();
        }

        if (currentWeaponIndex == prevWeaponIndex)
        {
            HandleWeaponChangeInput();
        }
    }

    /// <summary>
    /// Add event listeners
    /// </summary>
    private void OnEnable()
    {
        PickupEvent.AddListener(OnPickup);
        ClimbUpStartEvent.AddListener(OnClimbUpStarted);
        ClimbUpFinishEvent.AddListener(OnClimbUpFinished);
    }

    /// <summary>
    /// Handle climb start, rotate gun and set flag
    /// </summary>
    private void OnClimbUpStarted(ClimbUpStartEvent eventData)
    {
        isClimbing = true;
        StartCoroutine(RotateGunHolder(Quaternion.Euler(Vector3.zero), Quaternion.Euler(50f, 0f, 0f)));
    }

    /// <summary>
    /// Handle climb finishes, rotate gun back and unset flag
    /// </summary>
    private void OnClimbUpFinished(ClimbUpFinishEvent eventData)
    {
        isClimbing = false;
        StartCoroutine(RotateGunHolder(Quaternion.Euler(50f, 0f, 0f), Quaternion.Euler(Vector3.zero)));
    }

    /// <summary>
    /// Remove event listeners
    /// </summary>
    private void OnDisable()
    {
        PickupEvent.RemoveListener(OnPickup);
        ClimbUpStartEvent.RemoveListener(OnClimbUpStarted);
        ClimbUpFinishEvent.RemoveListener(OnClimbUpFinished);
    }

    /// <summary>
    /// Coroutine rotating current gun from start to target rotation over time
    /// </summary>
    /// <param name="startRot">Start rotation</param>
    /// <param name="targetRot">Target rotation</param>
    /// <returns>Coroutine</returns>
    private IEnumerator RotateGunHolder(Quaternion startRot, Quaternion targetRot)
    {
        float elapsedTime = 0f;

        while (elapsedTime < gunHolderRotationLength)
        {
            transform.localRotation = Quaternion.Slerp(startRot, targetRot, 
                animationCurve.Evaluate(elapsedTime / gunHolderRotationLength));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.localRotation = targetRot;
    }

    /// <summary>
    /// Change weapon if the corresponding input is received. Trigger animation, set flag to true.
    /// </summary>
    private void HandleWeaponChangeInput()
    {
        float scrollWheelAxisValue = JoystickAxisDataManager.AxisGetButtonDown("D-Pad Y Axis");

        if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") > .0f || scrollWheelAxisValue > .0f)
        {
            currentWeaponIndex = (currentWeaponIndex + 1) % weapons.Length;
            animator.SetTrigger("HolsterGun");
            isSwitchingWeapon = true;
        }
        else if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") < .0f || scrollWheelAxisValue < .0f)
        {
            if (currentWeaponIndex == 0)
            {
                currentWeaponIndex = weapons.Length - 1;
            }
            else
            {
                currentWeaponIndex = (currentWeaponIndex - 1) % weapons.Length;
            }

            animator.SetTrigger("HolsterGun");
            isSwitchingWeapon = true;
        }
    }

    /// <summary>
    /// Deactivate previous weapon, activate current, trigger pull gun animation.
    /// </summary>
    public void SwitchWeapon()
    {
        weapons[prevWeaponIndex].gameObject.SetActive(false);
        weapons[currentWeaponIndex].gameObject.SetActive(true);
        animator.SetTrigger("PullGun");
    }

    /// <summary>
    /// When new gun pull anim finished, set prev weapon to current weapon, unset weapon switching flag.
    /// </summary>
    public void FinishWeaponSwitch()
    {
        prevWeaponIndex = currentWeaponIndex;
        isSwitchingWeapon = false;
    }

    /// <summary>
    /// Weapon fire input logic. If the weapon is automatic, fire while fire button is held down.
    /// </summary>
    private void HandleFireInput()
    {
        if (weapons[currentWeaponIndex].IsAutomatic)
        {
            if (Input.GetButton("Fire1") || Input.GetAxis("Joystick Fire1") != 0f)
            {
                weapons[currentWeaponIndex].Shoot(firingSince);
                firingSince += Time.deltaTime;
            }
            else
            {
                firingSince = 0f;
            }
        }
        else
        {
            float joyFire1AxisValue = JoystickAxisDataManager.AxisGetButtonDown("Joystick Fire1");
            if (Input.GetButtonDown("Fire1") || joyFire1AxisValue != 0f)
            {
                weapons[currentWeaponIndex].Shoot(null);
            }
        }
    }
}