﻿using UnityEngine;

/// <summary>
/// Random scale and rotation for the muzzle flash, handling lifecycle
/// </summary>
public class MuzzleFlash : MonoBehaviour
{
    [SerializeField]
    private float initialLifeSpan = 0.1f;
    [SerializeField]
    private float minSize = 0.2f;
    [SerializeField]
    private float maxSize = 0.5f;

    private float lifeSpan;

    /// <summary>
    /// Set lifespan, random rotation and scale
    /// </summary>
    private void OnEnable()
    {
        lifeSpan = initialLifeSpan;
        float randomScale = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(randomScale, randomScale, 0f);
        transform.Rotate(Vector3.forward, Random.Range(0f, 360f));
    }

    /// <summary>
    /// Subtract elapsed time from lifespan, deactivate when no lifespan left
    /// </summary>
    private void Update()
    {
        if (lifeSpan <= 0f)
        {
            gameObject.SetActive(false);
        }

        lifeSpan -= Time.deltaTime;
    }
}
