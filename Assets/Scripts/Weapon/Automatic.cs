﻿using UnityEngine;

/// <summary>
/// Class for automatic weapons. Shoot with bullet spread.
/// </summary>
class Automatic : Weapon
{
    /// <summary>
    /// Shoot a raycast with the calculated spread applied.
    /// </summary>
    protected override void MakeShot()
    {
        weaponManager.ChangeAmmo(-1);

        if (Physics.Raycast(rayOrigin, CalculateBulletSpread(transform.rotation) * Vector3.forward, out hit,
            gunStats.range, gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}

