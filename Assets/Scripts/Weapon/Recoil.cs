﻿using UnityEngine;

/// <summary>
/// Recoil effects for weapons.
/// </summary>
public class Recoil : MonoBehaviour
{
    private static float camRecoil;
    private static float recoilRot;

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private float initialCamRecoil = 50f;
    [SerializeField]
    private float damping = 0.8f;
    [SerializeField]
    private float startRecoil = 5f;
    [SerializeField]
    private float maxRecoilX = -20f;
    [SerializeField]
    private float maxRecoilY = 20f;
    [SerializeField]
    private float recoilSpeed = 2f;
    [SerializeField]
    private AnimationCurve curve;
    [SerializeField]
    private float recoilKickBack = 0.5f;

    private float recoil;
    private Vector3 startPos;
    private Vector3 kickBackPos;

    /// <summary>
    /// Set a recoil amount to enable recoil effect
    /// </summary>
    public void StartRecoil()
    {
        maxRecoilY = Random.Range(-maxRecoilY, maxRecoilY);
        recoil = startRecoil;
        camRecoil += initialCamRecoil;
    }

    /// <summary>
    /// Called every frame to calculate recoil rotation and position. If the recoil has ended, 
    /// return the weapon to the starting position and rotation
    /// </summary>
    private void WeaponRecoil()
    {
        if (recoil > 0f)
        {

            Quaternion maxRecoil = Quaternion.Euler(-maxRecoilX, maxRecoilY, 0f);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, maxRecoil, Time.deltaTime * recoilSpeed);
            transform.localPosition = Vector3.Lerp(startPos, kickBackPos, curve.Evaluate(recoil / startRecoil));
            recoil -= Time.deltaTime;
        }
        else
        {
            recoil = 0f;
            transform.localPosition = startPos;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, Time.deltaTime * recoilSpeed * 0.5f);
        }
    }

    /// <summary>
    /// Camera recoil effect (currently disabled)
    /// </summary>
    private void CameraRecoil()
    {
        camRecoil *= 0.8f;
        recoilRot += camRecoil * 0.033f;
        recoilRot *= damping;
        float rot = Mathf.Clamp(recoilRot, -85, 85);

        cam.transform.Rotate(-rot, 0f, 0f, Space.Self);

        if (camRecoil <= 0.1f)
        {
            camRecoil = 0f;
        }

        if (recoilRot <= 0.1f)
        {
            recoilRot = 0f;
        }

        Debug.Log(string.Format("recoilRot:{0} rot:{1}", recoilRot, rot));
    }

    /// <summary>
    /// Set start and kickback positions of weapon
    /// </summary>
    private void Start()
    {
        startPos = transform.localPosition;
        kickBackPos = new Vector3(startPos.x, startPos.y, startPos.z - recoilKickBack);
    }

    /// <summary>
    /// Call WeaponRecoil() every frame
    /// </summary>
    private void Update()
    {
        WeaponRecoil();
        //CameraRecoil();
    }
}