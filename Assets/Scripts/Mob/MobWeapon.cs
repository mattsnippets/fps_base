﻿using UnityEngine;

/// <summary>
/// Abstract base class for all mob weapons.
/// </summary>
public abstract class MobWeapon : MonoBehaviour
{
    public abstract void Attack(Transform target);
    public abstract void StopAttack();
}