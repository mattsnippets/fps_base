﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/// <summary>
/// FOV script for mobs.
/// </summary>
public class FieldOfView : MonoBehaviour
{
    [SerializeField]
    private float viewRadius;
    [SerializeField]
    [Range(0f, 360f)]
    private float viewAngle;
    [SerializeField]
    private LayerMask targetMask;
    [SerializeField]
    private LayerMask obstacleMask;

    private List<Transform> visibleTargets = new List<Transform>();

    public float ViewRadius
    {
        get
        {
            return viewRadius;
        }
    }

    public float ViewAngle
    {
        get
        {
            return viewAngle;
        }
    }

    private void Start()
    {
        StartCoroutine(FindTargetsWithDelay(0.2f));
    }

    /// <summary>
    /// Wait a bit between FOV checks so the engine isn't overwhelmed with raycasts.
    /// </summary>
    /// <param name="delay">Timespan between FOV checks</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    /// <summary>
    /// Check for visible targets in the given radius.
    /// </summary>
    private void FindVisibleTargets()
    {
        visibleTargets.Clear();

        // Targets overlapping with radius sphere
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        foreach (Collider targetCollider in targetsInViewRadius)
        {
            Transform target = targetCollider.transform;
            Vector3 directionToTarget = (target.position - transform.position).normalized;

            // If target within the FOV angle
            if (Vector3.Angle(transform.forward, directionToTarget) < viewAngle / 2f)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                // If target isn't behind an obstacle
                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                }
            }
        }
    }

    /// <summary>
    /// Helper method that converts angle to direction vector.
    /// </summary>
    /// <param name="angleInDegrees">The angle in degrees</param>
    /// <param name="isAngleGlobal">True if the angle is global</param>
    /// <returns>Direction vector</returns>
    public Vector3 DirectionFromAngle(float angleInDegrees, bool isAngleGlobal)
    {
        if (!isAngleGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0f,
            Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    /// <summary>
    /// Returns visible targets.
    /// </summary>
    /// <returns>A redonly collection of targets within FOV</returns>
    public ReadOnlyCollection<Transform> GetVisibleTargets()
    {
        return new ReadOnlyCollection<Transform>(visibleTargets);
    }
}
