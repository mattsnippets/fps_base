﻿using UnityEngine;

/// <summary>
/// Bullet class for mob projectiles.
/// </summary>
public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float speed = 10f;
    [SerializeField]
    private int damage = 1;

    private float timeSinceSpawned;
    private float lifeTime = 15f;

    private void OnEnable()
    {
        timeSinceSpawned = 0f;
    }

    private void Update()
    {
        // Move bullet
        transform.Translate(0f, 0f, speed * Time.deltaTime);

        timeSinceSpawned += Time.deltaTime;

        if(timeSinceSpawned >= lifeTime)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// When the bullet's trigger collides with some other object, destroy the bullet if other is
    /// not a projectile, bullet spawner or door collider. If other has HealthManager, reduce its health.
    /// </summary>
    /// <param name="other">The other collider</param>
    private void OnTriggerEnter(Collider other)
    {
        Tags otherTags = other.GetComponent<Tags>();

        if (!otherTags.Projectile && !otherTags.BulletSpawner && !otherTags.DoorCollider)
        {
            gameObject.SetActive(false);
        }

        HealthManager targetHealthManager = other.gameObject.GetComponent<PlayerHealthManager>();

        if (targetHealthManager != null)
        {
            targetHealthManager.Damage(damage);
        }
    }
}