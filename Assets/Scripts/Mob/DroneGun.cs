﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Gun class for drone mobs.
/// </summary>
public class DroneGun : MobWeapon
{
    [SerializeField]
    private float attackCooldown = 2f;
    [SerializeField]
    private GameObject bulletPrefab;

    private AudioSource audioSource;
    private bool isAttacking = false;
    private Coroutine attackCoroutine;

    /// <summary>
    /// Set AudioSource reference.
    /// </summary>
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Start attack coroutine if not already attacking.
    /// </summary>
    public override void Attack(Transform target)
    {
        if (!isAttacking)
        {
            attackCoroutine = StartCoroutine(ShootTarget(target));
        }
    }

    /// <summary>
    /// Stop attack coroutine.
    /// </summary>
    public override void StopAttack()
    {
        if (isAttacking)
        {
            StopCoroutine(attackCoroutine);
            isAttacking = false;
        }
    }

    /// <summary>
    /// While coroutine is running, shoot bullets in forward direction, wait cooldown timespan between
    /// shots.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator ShootTarget(Transform target)
    {
        isAttacking = true;

        while (true)
        {
            GameObject bullet = ObjectPoolManager.Current.GetPooledObject(bulletPrefab);                
            bullet.transform.position = transform.position;
            bullet.transform.rotation = transform.rotation;
            bullet.transform.LookAt(target);
            bullet.SetActive(true);

            audioSource.Play();
            yield return new WaitForSeconds(attackCooldown);
        }
    }
}
