﻿using UnityEngine;

/// <summary>
/// Mob health manager class.
/// </summary>
public class MobHealthManager : HealthManager
{
    [SerializeField]
    private GameObject deathExplosion;

    private int previousHealth;

    private void Start()
    {
        previousHealth = health;
    }

    /// <summary>
    /// Set new health value when damaged, if health below 0 instantiate explosion, deactivate then
    /// destroy mob.
    /// </summary>
    /// <param name="damage">Amount of damage</param>
    public override void Damage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            GameObject deathExplosionGo = ObjectPoolManager.Current.GetPooledObject(deathExplosion);
            deathExplosionGo.transform.position = transform.position;
            deathExplosionGo.transform.rotation = Quaternion.identity;
            deathExplosionGo.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Set new health value when healed.
    /// </summary>
    /// <param name="heal">Amount of healing</param>
    public override void Heal(int heal)
    {
        health += heal;
    }

    /// <summary>
    /// Check if the mob has been injured since the last check.
    /// </summary>
    /// <returns>True if current health is less than previous health</returns>
    public bool HasBeenInjured()
    {
        if (health < previousHealth)
        {
            previousHealth = health;
            return true;
        }
        else
        {
            return false;
        }
    }
}
