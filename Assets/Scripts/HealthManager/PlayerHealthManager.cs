﻿using Mattsnippets.EventSystem;
using UnityEngine;

/// <summary>
/// Player health manager class.
/// </summary>
public class PlayerHealthManager : HealthManager
{
    [SerializeField]
    private AudioClip injurySound;

    private AudioSource audioSource;
    private PlayerHealthChangeEvent healthChangeEvent;

    public int MaxHealth { get; } = 999;

    /// <summary>
    /// Init player health change event.
    /// </summary>
    private void Awake()
    {
        healthChangeEvent = new PlayerHealthChangeEvent(health, false);
    }

    /// <summary>
    /// Broadcast health value on start, set AudioSource reference, add health pickup event listener.
    /// </summary>
    private void Start()
    {
        healthChangeEvent.Health = health;
        healthChangeEvent.IsDamage = false;
        healthChangeEvent.Fire();
        audioSource = GetComponent<AudioSource>();
        PickupEvent.AddListener(OnPickup);
    }

    /// <summary>
    /// Remove pickup event listener.
    /// </summary>
    private void OnDisable()
    {
        PickupEvent.RemoveListener(OnPickup);
    }

    /// <summary>
    /// Set new health value when damaged, broadcast the new value, play injury sound. If health below
    /// 0 trigger game over event.
    /// </summary>
    /// <param name="amount">Amount of danage</param>
    public override void Damage(int amount)
    {
        health -= amount;
        healthChangeEvent.Health = health;
        healthChangeEvent.IsDamage = true;
        healthChangeEvent.Fire();
        audioSource.PlayOneShot(injurySound);

        if (health <= 0)
        {
            gameObject.SetActive(false);
            new GameOverEvent().Fire();
        }
    }

    /// <summary>
    /// Set new health value when healed, limit health to maxhealth, boradcast new value.
    /// </summary>
    /// <param name="heal">Amount of healing</param>
    public override void Heal(int amount)
    {
        int increasedHealth = health + amount;
        health = (increasedHealth <= MaxHealth) ? increasedHealth : MaxHealth;
        healthChangeEvent.Health = health;
        healthChangeEvent.IsDamage = false;
        healthChangeEvent.Fire();
    }

    /// <summary>
    /// If health pickup event received, heal player.
    /// </summary>
    /// <param name="eventData">Event data</param>
    private void OnPickup(PickupEvent eventData)
    {
        if (eventData.Type == PickupType.Health)
        {
            Heal(eventData.Amount);
        }
    }
}
