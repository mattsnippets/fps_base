﻿using UnityEngine;

/// <summary>
/// Abstract health manager base class.
/// </summary>
public abstract class HealthManager : MonoBehaviour
{
    [SerializeField]
    protected int health;

    public int Health
    {
        get
        {
            return health;
        }
    }

    public abstract void Damage(int amount);
    public abstract void Heal(int amount);
}
