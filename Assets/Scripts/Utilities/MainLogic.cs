﻿using UnityEngine;

/// <summary>
/// Main logic script for game manager.
/// </summary>
public class MainLogic : MonoBehaviour
{
    private GuiManager guiManager;

    /// <summary>
    /// Set target framerate.
    /// </summary>
    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    /// <summary>
    /// Set GuiManager reference.
    /// </summary>
    private void Start()
    {
        guiManager = GetComponent<GuiManager>();
    }

    /// <summary>
    /// Handle escape key.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxis("Joystick Back") > 0f)
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.F1) || JoystickAxisDataManager.AxisGetButtonDown("Joystick Start") > 0f)
        {
            guiManager.ToggleHelpScreen();
        }
    }
}
