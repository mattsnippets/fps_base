﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Sliding door logic script.
/// </summary>
public class DoorLogic : MonoBehaviour
{
    [SerializeField]
    private float doorMoveDuration;
    [SerializeField]
    private GameObject movingPart;
    [SerializeField]
    private AudioClip openSound;
    [SerializeField]
    private AudioClip closeSound;

    private Vector3 openPosition;
    private Vector3 closedPosition;
    private bool isOpen;
    private bool isOpening;
    private AudioSource audioSource;
    private Coroutine moveDoor;

    /// <summary>
    /// Set closed and open position and AudioSource reference.
    /// </summary>
    private void Awake()
    {
        closedPosition = movingPart.transform.position;
        openPosition = movingPart.transform.position + (movingPart.transform.forward * -3.8f);
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// If the door is in the opposite state than required and isn't moving right now start the 
    /// move coroutine in the right direction.
    /// </summary>
    private void LateUpdate()
    {
        if (isOpen != isOpening && moveDoor == null)
        {
            Debug.Log("MoveDoor " + isOpening);
            moveDoor = isOpening ? StartCoroutine(MoveDoor(closedPosition, openPosition))
                : StartCoroutine(MoveDoor(openPosition, closedPosition));
        }
    }

    /// <summary>
    /// Lerp door to target position.
    /// </summary>
    /// <param name="startPos">Start position</param>
    /// <param name="targetPos">Target position</param>
    /// <returns>Coroutine</returns>
    private IEnumerator MoveDoor(Vector3 startPos, Vector3 targetPos)
    {
        float elapsed = 0f;

        while (elapsed < doorMoveDuration)
        {
            movingPart.transform.position = Vector3.Lerp(startPos, targetPos, elapsed / doorMoveDuration);
            elapsed += Time.deltaTime;
            yield return null;
        }

        movingPart.transform.position = targetPos;
        moveDoor = null;
        isOpen = !isOpen;
    }

    /// <summary>
    /// Set to opening if trigger collides with player.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Tags>().Player)
        {
            isOpening = true;
            audioSource.PlayOneShot(openSound);
        }
    }

    /// <summary>
    /// Set to closing if player leaves trigger.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Tags>().Player)
        {
            isOpening = false;
            audioSource.PlayOneShot(closeSound);
        }
    }
}
