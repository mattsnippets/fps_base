﻿using UnityEngine;

/// <summary>
/// Tag utility class to allow handling multiple tags.
/// </summary>
public class Tags : MonoBehaviour
{
    public bool Player;
    public bool Climbable;
    public bool Ladder;
    public bool Projectile;
    public bool BulletSpawner;
    public bool DoorCollider;
}
