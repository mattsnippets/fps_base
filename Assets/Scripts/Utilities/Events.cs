﻿namespace Mattsnippets.EventSystem
{
    public class PickupEvent : Event<PickupEvent>
    {
        public int Amount;
        public PickupType Type;

        public PickupEvent(PickupType type, int amount)
        {
            Type = type;
            Description = type.ToString();
            Amount = amount;
        }
    }

    public class PlayerHealthChangeEvent : Event<PlayerHealthChangeEvent>
    {
        public int Health;
        public bool IsDamage;

        public PlayerHealthChangeEvent(int health, bool isDamage)
        {
            Health = health;
            IsDamage = isDamage;
        }
    }

    public class PlayerAmmoChangeEvent : Event<PlayerAmmoChangeEvent>
    {
        public int Ammo;

        public PlayerAmmoChangeEvent(int ammo)
        {
            Ammo = ammo;
        }
    }

    public class ToggleLadderMovementEvent : Event<ToggleLadderMovementEvent>
    {
        public bool IsMovementEnabled;

        public ToggleLadderMovementEvent(bool isMovementEnabled)
        {
            IsMovementEnabled = isMovementEnabled;
        }
    }

    public class GameOverEvent : Event<GameOverEvent> { }

    public class ClimbUpTriggerEvent : Event<ClimbUpTriggerEvent> { }

    public class ClimbUpStartEvent : Event<ClimbUpStartEvent> { }

    public class ClimbUpFinishEvent : Event<ClimbUpFinishEvent> { }
}
