﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Attach to a GameObjet to make it fade out then deactivate it.
/// </summary>
public class FadeOutDeactivator : MonoBehaviour
{
    [SerializeField]
    private float fadeSpeed = 1f;
    [SerializeField]
    private float waitBeforeFade = 1f;

    private Renderer ownRenderer;
    private Color originalColor;

    /// <summary>
    /// Get renderer reference and store original color
    /// </summary>
    private void Awake()
    {
        ownRenderer = GetComponent<Renderer>();
        originalColor = ownRenderer.material.GetColor("_TintColor");
    }

    /// <summary>
    /// Set color back to original color, start fade out coroutine
    /// </summary>
    private void OnEnable()
    {
        ownRenderer.material.SetColor("_TintColor", originalColor);
        StartCoroutine(FadeOutDeactivate());
    }

    /// <summary>
    /// Coroutine which fades out then deactivates the GameObject
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator FadeOutDeactivate()
    {
        yield return new WaitForSeconds(waitBeforeFade);

        Color currentColor = ownRenderer.material.GetColor("_TintColor");

        while (currentColor.a > 0f)
        {
            currentColor.a -= Time.deltaTime * fadeSpeed;
            ownRenderer.material.SetColor("_TintColor", currentColor);
            yield return null;
        }

        gameObject.SetActive(false);
    }
}
