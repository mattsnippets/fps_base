﻿using UnityEngine;

/// <summary>
/// Handles the reference and sets the size of the gun's separate render texture.
/// </summary>
public class GunCamera : MonoBehaviour
{
    [SerializeField]
    private RenderTexture renderTexture;

    private void Awake()
    {
        renderTexture.width = Screen.width;
        renderTexture.height = Screen.height;
    }
}
