﻿using Mattsnippets.EventSystem;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// GUI manager class.
/// </summary>
public class GuiManager : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup gameOverPanel;
    [SerializeField]
    private Text playerHealthText;
    [SerializeField]
    private Image damageIndicator;
    [SerializeField]
    private Text playerAmmoText;
    [SerializeField]
    private float damageIndicatorFlashLength;
    [SerializeField]
    private AudioListener guiAudioListener;
    [SerializeField]
    private CanvasGroup helpScreen;

    /// <summary>
    /// Add event listeners.
    /// </summary>
    private void OnEnable()
    {
        GameOverEvent.AddListener(OnGameOver);
        PlayerHealthChangeEvent.AddListener(OnPlayerHealthChange);
        PlayerAmmoChangeEvent.AddListener(OnPlayerAmmoChange);
    }

    /// <summary>
    /// Remove event listeners.
    /// </summary>
    private void OnDisable()
    {
        GameOverEvent.RemoveListener(OnGameOver);
        PlayerHealthChangeEvent.RemoveListener(OnPlayerHealthChange);
        PlayerAmmoChangeEvent.RemoveListener(OnPlayerAmmoChange);
    }

    /// <summary>
    /// Initialize damage indicator's alpha (required for CrossFadeAlpha()).
    /// </summary>
    private void Awake()
    {
        damageIndicator.canvasRenderer.SetAlpha(0f);
    }

    /// <summary>
    /// Toggle help screen visibility.
    /// </summary>
    public void ToggleHelpScreen()
    {
        helpScreen.alpha = (helpScreen.alpha > 0f) ? 0f : 1f;
    }
    
    /// <summary>
    /// Event handler for level restart button.
    /// </summary>
    public void OnRestartButtonClick()
    {
        guiAudioListener.enabled = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Game over event handler.
    /// </summary>
    /// <param name="eventData">Event data</param>
    private void OnGameOver(GameOverEvent eventData)
    {
        gameOverPanel.alpha = 1f;
        gameOverPanel.interactable = true;
        guiAudioListener.enabled = true;
    }

    /// <summary>
    /// Player health change event handler. Shows damage indicator and updates health text.
    /// </summary>
    /// <param name="eventData">Event data</param>
    private void OnPlayerHealthChange(PlayerHealthChangeEvent eventData)
    {
        if (eventData.IsDamage)
        {
            StartCoroutine(FlashDamageIndicator());
        }

        playerHealthText.text = eventData.Health.ToString();
    }

    /// <summary>
    /// Event handler for player ammo change.
    /// </summary>
    /// <param name="eventData">Event data</param>
    private void OnPlayerAmmoChange(PlayerAmmoChangeEvent eventData)
    {
        playerAmmoText.text = eventData.Ammo.ToString();
    }

    /// <summary>
    /// Flash damage indicator panel for a short time.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator FlashDamageIndicator()
    {
        damageIndicator.CrossFadeAlpha(120f, damageIndicatorFlashLength, false);
        yield return new WaitForSeconds(damageIndicatorFlashLength);
        damageIndicator.CrossFadeAlpha(0f, damageIndicatorFlashLength, false);
    }
}
